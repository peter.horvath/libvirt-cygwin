#!/bin/bash
git remote remove gitlab
git remote remove origin
git remote add gitlab git@gitlab.com:peter.horvath/libvirt-cygwin.git
git remote add origin git@gitlab.com:libvirt/libvirt.git
git fetch --all --prune --tags
git branch master --set-upstream-to remotes/gitlab/master
